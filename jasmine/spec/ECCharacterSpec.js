describe('EverCraft Character', function () {
	var character;
	var opponent;
	
	beforeEach(function () {
		character = new ECCharacter();
		opponent = new ECCharacter();
	});
	
	describe('Iteration #1', function () {
		it('has a name', function () {
			character.setName('Roghar');
			expect(character.name).toEqual('Roghar');
		});
		
		it('has an alignment', function () {
			character.setAlignment('Good');
			expect(character.alignment).toEqual('Good');
		});
		it('has an alignment of Good, Neutral, or Evil', function () {
			expect(function () { character.setAlignment('Anything'); })
				.toThrow(new Error('Invalid alignment'));
		});
		
		it('has a default armor class of 10', function () {
			expect(character.armorClass()).toEqual(10);
		})
		it('has 5 hit points by default', function () {
			expect(character.hitPoints).toEqual(5);
		});
		it('rolls a 20-sided dice to determine its attack', function () {
			expect(function () { character.attack(opponent, 0); })
				.toThrow(new Error('Attack roll must be between 1 and 20'));
			expect(function () { character.attack(opponent, 21); })
				.toThrow(new Error('Attack roll must be between 1 and 20'));
		});
		it('successfully attacks if its roll exceeds the armor class of an opponent', function () {
			var attack = character.attack(opponent, 11);
			expect(attack).toBeTruthy();
		});
		
		it('damages opponent on successful attack', function () {
			var attack = character.attack(opponent, 11);
			expect(opponent.hitPoints).toEqual(4);
		});
		it('deals double damage on a critical hit (rolls 20)', function () {
			var attack = character.attack(opponent, 20);
			expect(opponent.hitPoints).toEqual(3);
		});
		
		it('has Strength (default of 10)', function () {
			expect(character.str).toEqual(10);
		});
		it('has Dexterity (default of 10)', function () {
			expect(character.dex).toEqual(10);
		});
		it('has Constitution (default of 10)', function () {
			expect(character.con).toEqual(10);
		});
		it('has Intelligence (default of 10)', function () {
			expect(character.int).toEqual(10);
		});
		it('has Wisdom (default of 10)', function () {
			expect(character.wis).toEqual(10);
		});
		it('has Charisma (default of 10)', function () {
			expect(character.cha).toEqual(10);
		});
		
		it('has a Strength modifier', function () {
			expect(character.strMod()).toEqual(0);
		});
		it('has a Dexterity modifier', function () {
			expect(character.dexMod()).toEqual(0);
		});
		it('has a Constitution modifier', function () {
			expect(character.conMod()).toEqual(0);
		});
		it('has a Intelligence modifier', function () {
			expect(character.intMod()).toEqual(0);
		});
		it('has a Wisdom modifier', function () {
			expect(character.wisMod()).toEqual(0);
		});
		it('has a Charisma modifier', function () {
			expect(character.chaMod()).toEqual(0);
		});
		
		it('adds Strength modifier to attack rolls', function () {
			character.str = 12;
			var attack = character.attack(opponent, 10);
			expect(attack).toBeTruthy();
		});
		it('adds Strength modifieer to damage rolls', function () {
			character.str = 12;
			var attack = character.attack(opponent, 10);
			expect(opponent.hitPoints).toEqual(3);
		});
		it('deals a minimum of 1 damage despite Strength modifier', function () {
			character.str = 1;
			var attack = character.attack(opponent, 19);
			expect(opponent.hitPoints).toEqual(4);
		});
		
		it('adds Deterity modifier to armor class', function () {
			character.dex = 14;
			expect(character.armorClass()).toBe(12);
		});
		
		it('adds Constitution modifier to hit points', function () {
			character.con = 16;
			expect(character.hitPoints).toEqual(8);
		});
		it('has a minimum of 1 hit point', function () {
			character.con = 1;
			expect(character.hitPoints).toEqual(1);
		});
		
		it('gains experience when successfully attacking', function () {
			character.attack(opponent, 11);
			expect(character.experience).toEqual(10);
		});
		
		it('starts at level 1', function () {
			expect(character.level).toEqual(1);
		})
		it('gains a level after every 1000 experience points', function () {
			character.gainsXP(2000);
			expect(character.level).toEqual(3);
		});
		it('gains 5 hit points plus Constitution modifier for each level', function () {
			character.gainsXP(2000);
			expect(character.hitPoints).toEqual(15);
		});
		it('adds +1 to attack rolls for every even level', function () {
			character.gainsXP(3000);
			var attack = character.attack(opponent, 9);
			expect(attack).toBeTruthy();
		});
	});
	describe('Iteration #2', function () {
		describe('Fighter class', function () {
			var fighter;
			
			beforeEach(function () {
				fighter = new ECFighter();
			});
			
			it('gains +1 to attack for every level', function () {
				fighter.gainsXP(2000);
				expect(fighter.attackBonus()).toEqual(3);
			});
			it('has 10 hit points by default', function () {
				expect(fighter.hitPoints).toEqual(10);
			});
			it('gains 10 hit points per level', function () {
				fighter.gainsXP(2000);
				expect(fighter.hitPoints).toEqual(30);
			});
		});
		describe('Rogue class', function () {
			var rogue;
			
			beforeEach(function () {
				rogue = new ECRogue();
			})
			
			it('does triple damage on critical strikes', function () {
				var attack = rogue.attack(opponent, 20);
				expect(opponent.hitPoints).toEqual(2);
			})
			it('ignores Dexterity modifier of opponent (if positive) when attacking', function () {
				opponent.dex = 18;
				var attack = rogue.attack(opponent, 11);
				expect(attack).toBeTruthy();
			});
			it('add Deterity modifier to attacks in lieu of Strength modifier', function () {
				rogue.dex = 12;
				var attack = rogue.attack(opponent, 10);
				expect(attack).toBeTruthy();
			});
			it('cannot have a Good alignment', function () {
				expect(function () { rogue.setAlignment('Good'); })
					.toThrow(new Error('Invalid alignment'));
			});
		});
		describe('Monk class', function () {
			var monk;
			
			beforeEach(function () {
				monk = new ECMonk();
			});
			
			it('has 6 hit points by default', function () {
				expect(monk.hitPoints).toEqual(6);
			});
			it('gains 6 hit points per level', function () {
				monk.gainsXP(2000);
				expect(monk.hitPoints).toEqual(18);
			});
			it('does 3 points of damage instead of 1 when successfully attacking', function () {
				var attack = monk.attack(opponent, 11);
				expect(opponent.hitPoints).toEqual(2);
			});
			it('adds Wisdom modifier (if positive) to armor class in addition to Dexterity modifier', function () {
				monk.dex = 14;
				monk.wis = 12;
				expect(monk.armorClass()).toBe(13);
				monk.wis = 7;
				expect(monk.armorClass()).toBe(12);
			});
			it('add +1 to attack rolls for every 2nd and 3rd level', function () {
				monk.gainsXP(3000);
				expect(monk.attackBonus()).toEqual(3);
			});
		});
		describe('Paladin class', function () {
			var paladin;
			
			beforeEach(function () {
				paladin = new ECPaladin();
			});
			
			it('has 8 hit points by default', function () {
				expect(paladin.hitPoints).toEqual(8);
			});
			it('gains 8 hit points per level', function () {
				paladin.gainsXP(2000);
				expect(paladin.hitPoints).toEqual(24);
			});
			it('does +2 to attack against evil opponents', function () {
				opponent.setAlignment('Evil');
				var attack = paladin.attack(opponent, 9);
				expect(attack).toBeTruthy();
			});
			it('does +2 to damage against evil opponents', function () {
				opponent.setAlignment('Evil');
				paladin.attack(opponent, 9);
				expect(opponent.hitPoints).toEqual(2);
			});
			it('deals triple damage on critical hits', function () {
				opponent.setAlignment('Evil');
				paladin.attack(opponent, 20);
				expect(opponent.hitPoints).toEqual(-4);
			});
			it('adds +1 to attack rolls for every level', function () {
				paladin.gainsXP(2000);
				expect(paladin.attackBonus()).toEqual(3);
			});
			it('can only have a Good alignment', function () {
				expect(function () { paladin.setAlignment('Neutral'); })
					.toThrow(new Error('Invalid alignment'));
				expect(function () { paladin.setAlignment('Evil'); })
					.toThrow(new Error('Invalid alignment'));
			});
		});
	});
	describe('Iteration #3', function () {
		it('defaults to Human race', function () {
			expect(character.race).toEqual('Human');
		});
		
		describe('Orc race', function () {
			var orc;
			
			beforeEach(function () {
				orc = new ECCharacter();
				orc.race = 'Orc';
			});
			
			it('has a +2 Stength modifier', function () {
				expect(orc.strMod()).toEqual(2);
			});
			it('has a -1 Intelligence modifier', function () {
				expect(orc.intMod()).toEqual(-1);
			});
			it('has a -1 Wisdom modifier', function () {
				expect(orc.wisMod()).toEqual(-1);
			});
			it('has a -1 Charisma modifier', function () {
				expect(orc.chaMod()).toEqual(-1);
			});
			it('has a +2 armor class bonus', function () {
				expect(orc.armorClass()).toEqual(12);
			});
		});
		describe('Dwarf race', function () {
			var dwarf, orc;
			
			beforeEach(function () {
				dwarf = new ECCharacter();
				dwarf.race = 'Dwarf';
				
				orc = new ECCharacter();
				orc.race = 'Orc';
			});
			
			it('has a +1 Constitution modifier', function () {
				expect(dwarf.conMod()).toEqual(1);
			});
			it('has a -1 Charisma modifier', function () {
				expect(dwarf.chaMod()).toEqual(-1);
			});
			it('gets double the Constitution modifier (if positive) to hit points', function () {
				expect(dwarf.hitPoints).toEqual(7);
			});
			it('gains +2 attack and damage versus orcs', function () {
				var attack = dwarf.attack(orc, 11);
				expect(attack).toBeTruthy();
				expect(orc.hitPoints).toEqual(2);
			});
		});
		describe('Elf race', function () {
			var elf, orc;
			
			beforeEach(function () {
				elf = new ECCharacter();
				elf.race = 'Elf';
				
				orc = new ECCharacter();
				orc.race = 'Orc';
			});
			
			it('has a +1 Deterity modifier', function () {
				expect(elf.dexMod()).toEqual(1);
			});
			it('has a -1 Constitution modifier', function () {
				expect(elf.conMod()).toEqual(-1);
			});
			it('add 1 to range for critical hits', function () {
				elf.attack(opponent, 19);
				expect(opponent.hitPoints).toEqual(3);
			});
			it('gains +2 armor class when being attacked by orcs', function () {
				var attack = orc.attack(elf, 11);
				expect(attack).toBeFalsy();
			});
		});
		describe('Halfling race', function () {
			var halfling, human;
			
			beforeEach(function () {
				halfling = new ECCharacter();
				halfling.race = 'Halfling';
				
				human = new ECCharacter();
			});
			
			it('has a +1 Dexterity modifier', function () {
				expect(halfling.dexMod()).toEqual(1);
			});
			it('has a -1 Strength modifier', function () {
				expect(halfling.strMod()).toEqual(-1);
			});
			it('gains +2 armor class when attacked by non-halflings', function () {
				var attack = human.attack(halfling, 12);
				expect(attack).toBeFalsy();
			});
		});
	});
	describe('Iteration #4', function () {
		describe('Weapons', function () {
			var longsword = new Longsword();
			var magicWaraxe = new Waraxe(2);
			var elvenLongsword = new ElvenLongsword();
			var nunchucks = new Nunchucks();
			
			describe('Longsword', function () {
				it('does 5 points of damage', function () {
					expect(longsword.weaponDamage()).toEqual(5);
				})
			});
			describe('+2 Waraxe', function () {
				it('does 6 points of damage', function () {
					expect(magicWaraxe.weaponDamage()).toEqual(6);
				});
				it('has a +2 attack bonus', function () {
					expect(magicWaraxe.totalAttack()).toEqual(2);
				});
				it('has a +2 damage bonus', function () {
					var dmgBonus = magicWaraxe.totalDamage() - magicWaraxe.weaponDamage();
					expect(dmgBonus).toEqual(2);
				});
				it('triples critical hit damage', function () {
					character.weapon = magicWaraxe;
					character.attack(opponent, 20);
					expect(opponent.hitPoints).toEqual(-19);
				});
			});
			describe('Elven longsword', function () {
				var elf, human, orc;
				
				beforeEach(function () {
					elf = new ECCharacter();
					elf.race = 'Elf';
					
					human = new ECCharacter();
					
					orc = new ECCharacter();
					orc.race = 'Orc';
				});
				
				it('does 5 points of damage', function () {
					expect(elvenLongsword.weaponDamage()).toEqual(5);	
				});
				it('has a +1 attack bonus', function () {
					expect(elvenLongsword.totalAttack()).toEqual(1);
				});
				it('has a +1 damage bonus', function () {
					var dmgBonus = elvenLongsword.totalDamage() - elvenLongsword.weaponDamage();
					expect(dmgBonus).toEqual(1);
				});
				it('has a +2 attack bonus when wielded by an elf', function () {
					expect(elvenLongsword.totalAttack(elf)).toEqual(2);
				});
				it('has a +2 damage bonus when wielded by an elf', function () {
					var dmgBonus = elvenLongsword.totalDamage(elf) - elvenLongsword.weaponDamage();
					expect(dmgBonus).toEqual(2);
				});
				it('has a +2 attack bonus when wielded against an orc', function () {
					expect(elvenLongsword.totalAttack(human, orc)).toEqual(2);
				});
				it('has a +2 damage bonus when wielded against an orc', function () {
					var dmgBonus = elvenLongsword.totalDamage(human, orc) - elvenLongsword.weaponDamage();
					expect(dmgBonus).toEqual(2);
				});
				it('has a +5 attack bonus when wielded by an elf against an orc', function () {
					expect(elvenLongsword.totalAttack(elf, orc)).toEqual(5);
				});
				it('has a +5 damage bonus when wielded by an elf against an orc', function () {
					var dmgBonus = elvenLongsword.totalDamage(elf, orc) - elvenLongsword.weaponDamage();
					expect(dmgBonus).toEqual(5);
				});
			});
			describe("Nunchucks", function () {
				it('does 6 points of damage', function () {
					expect(nunchucks.weaponDamage()).toEqual(6);
				});
				it('has a -4 attack penalty when wielded by a non-monk', function () {
					var monk = new ECMonk();
					expect(nunchucks.totalAttack(monk)).toEqual(0);
					expect(nunchucks.totalAttack(character)).toEqual(-4);
				});
			});
		});
		describe('Armor', function () {
			var leatherArmor = new LeatherArmor();
			var plateArmor = new PlateArmor();
			var elvenChainMail = new ElvenChainMail();
			var shield = new Shield();
			
			describe('Leather armor', function () {
				it('has a +2 armor class bonus', function () {
					character.armor = leatherArmor;
					expect(leatherArmor.totalArmor()).toEqual(2);
					expect(character.armorClass()).toEqual(12);
				});
			});
			describe('Plate armor', function () {
				it('has a +8 armor class bonus', function () {
					expect(plateArmor.totalArmor()).toEqual(8);
				});
				it('can only be worn by fighters of any race', function () {
					var fighter = new ECFighter();
					fighter.armor = plateArmor;
					expect(fighter.armorClass()).toEqual(18);
					expect(function () { character.armor = plateArmor; })
						.toThrow(new Error('Cannot equip armor.'));
				});
				it('can only be worn by dwarves of any class', function () {
					var paladin = new ECPaladin();
					paladin.race = 'Dwarf';
					paladin.armor = plateArmor;
					expect(paladin.armorClass()).toEqual(18);
				});
			});
			describe('Elven chain mail', function () {
				it('has a +3 armor class bonus', function () {
					expect(elvenChainMail.totalArmor()).toEqual(5);
				});
				it('has a +8 armor class bonus if worn by an elf', function () {
					character.race = 'Elf';
					character.armor = elvenChainMail;
					expect(character.armorClass()).toEqual(19);
				});
				it('has a +1 attack bonus if worn by an elf', function () {
					character.race = 'Elf';
					character.armor = elvenChainMail;
					expect(character.equipmentBonus()).toEqual(1);
				});
			});
			describe('Shield', function () {
				it('has a +3 armor class bonus', function () {
					expect(shield.totalArmor()).toEqual(3);
				})
				it('has a -4 attack penalty', function () {
					character.armor = shield;
					expect(character.equipmentBonus()).toEqual(-4);
				});
				it('has a -2 attack penalty for fighters', function () {
					var fighter = new ECFighter();
					fighter.armor = shield;
					expect(fighter.equipmentBonus()).toEqual(-2);
				});
			});
		});
		describe('Items', function () {
			var ringOfProtection = new RingOfProtection();
			var beltOfGiantStrength = new BeltOfGiantStrength();
			var amuletOfTheHeavens = new AmuletOfTheHeavens();
			
			describe('Ring of protection', function () {
				it('has a + armor class bonus', function () {
					character.items.push(ringOfProtection);
					expect(character.armorClass()).toEqual(12);
				});
			});
			describe('Belt of giant strength', function () {
				it('grants +4 to Strength', function () {
					character.items.push(beltOfGiantStrength);
					expect(character.str).toEqual(14);
				})
			});
			describe('Amulet of the heavens', function () {
				it('has a +1 attack bonus against Neutral enemies', function () {
					character.items.push(amuletOfTheHeavens);
					expect(character.equipmentBonus(opponent)).toEqual(1);
				});
				it('has a +2 attack bonus against Evil enemies', function () {
					character.items.push(amuletOfTheHeavens);
					opponent.alignment = 'Evil';
					expect(character.equipmentBonus(opponent)).toEqual(2);
				});
				it('doubles attack bonuses if worn by a Paladin', function () {
					var paladin = new ECPaladin();
					paladin.items.push(amuletOfTheHeavens);
					expect(paladin.equipmentBonus(opponent)).toEqual(2);
					opponent.alignment = 'Evil';
					expect(paladin.equipmentBonus(opponent)).toEqual(4);
				});
			});
		});
	});
});



























