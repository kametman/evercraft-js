function ECMonk () {
	ECCharacter.call();
	
	this.className = 'Monk';
	
	this.baseHitPoints = 6;
	this.hitPoints = 6;
	
	this.baseDamage = 3;
}

ECMonk.prototype = new ECCharacter();
ECMonk.prototype.constructor = ECMonk;

ECMonk.prototype.attackBonus = function (opponent) {
	return parseInt(this.level / 2) + parseInt(this.level / 3) + this.strMod();
}
ECMonk.prototype.armorClass = function(ignoreDex, opponent) {
	if (ignoreDex && this.dexMod() > 0) { return this.baseArmorClass(); }
	else { return this.baseArmorClass(opponent) + this.dexMod() + (this.wisMod() > 0 ? this.wisMod() : 0); }
};

