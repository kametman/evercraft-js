function ECPaladin () {
	ECCharacter.call();
	
	this.className = 'Paladin';
	this.alignment = 'Good';
	
	this.baseHitPoints = 8;
	this.hitPoints = 8;
};

ECPaladin.prototype = new ECCharacter();
ECPaladin.prototype.constructor = ECPaladin;

ECPaladin.prototype.setAlignment = function (alignment) {
	if (alignment == 'Good') {
		this.alignment = alignment;
	}
	else { throw new Error('Invalid alignment'); }
};

ECPaladin.prototype.attackBonus = function (opponent) {
	var isEvil = (opponent != null && opponent.alignment == 'Evil') ? 2 : 0;
	return this.level + this.strMod() + isEvil;
}
ECPaladin.prototype.damageDealt = function (opponent) {
	var isEvil = (opponent != null && opponent.alignment == 'Evil') ? 2 : 0;
	return this.weaponDamage() + this.strMod() + isEvil;
};
ECPaladin.prototype.criticalMultiplier = function (opponent) {
	var crit = this.critical;
	if (opponent != null && opponent.alignment == 'Evil') { crit += 1; }
	return crit;
}


