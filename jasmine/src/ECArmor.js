function ECArmor(bonus) {
	this.bonus = bonus || 0;
	this.armorClass = 0;
};
ECArmor.prototype.totalArmor = function () {
	return this.armorClass + this.bonus;	
};
ECArmor.prototype.totalAttack = function () {
	return 0;
}
ECArmor.prototype.canEquip = function () {
	return true;
};

function LeatherArmor(bonus) {
	ECArmor.call(this, bonus);
	this.armorClass = 2;
};
LeatherArmor.prototype = new ECArmor();
LeatherArmor.prototype.constructor = LeatherArmor;

function PlateArmor(bonus) {
	ECArmor.call(this, bonus);
	this.armorClass = 8;
};
PlateArmor.prototype = new ECArmor();
PlateArmor.prototype.constructor = PlateArmor;
PlateArmor.prototype.canEquip = function (wearer) {
	if (wearer.className == 'Fighter') { return true; }
	else if (wearer.race == 'Dwarf') { return true; }
	else { return false; }
};

function ElvenChainMail(bonus) {
	ECArmor.call(this, bonus);
	this.armorClass = 5;
}
ElvenChainMail.prototype = new ECArmor();
ElvenChainMail.prototype.constructor = ElvenChainMail;
ElvenChainMail.prototype.totalArmor = function (wearer) {
	var acBonus = 0;
	if (wearer != null && wearer.race == 'Elf') { acBonus = 3; }
	return this.armorClass + this.bonus + acBonus;
}
ElvenChainMail.prototype.totalAttack = function (wearer) {
	var atkBonus = 0;
	if (wearer != null && wearer.race == 'Elf') { atkBonus = 1; }
	return this.bonus + atkBonus;
}

function Shield(bonus) {
	ECArmor.call(this, bonus);
	this.armorClass = 3;
}
Shield.prototype = new ECArmor();
Shield.prototype.constructor = Shield;
Shield.prototype.totalAttack = function (wearer) {
	var atkBonus = -4;
	if (wearer != null && wearer.className == 'Fighter') { atkBonus = -2; }
	return this.bonus + atkBonus;
};