function ECItem(bonus) {
	this.bonus = bonus || 0;
	
	this.armorClass = 0;
	
	this.str = 0;
};
ECItem.prototype.totalStr = function () {
	return this.str;
}
ECItem.prototype.totalArmor = function () {
	return this.armorClass + this.bonus;	
};
ECItem.prototype.totalAttack = function () {
	return 0;
}

function RingOfProtection(bonus) {
	ECItem.call(this, bonus);
	
	this.armorClass = 2;
}
RingOfProtection.prototype = new ECItem();
RingOfProtection.prototype.constructor = RingOfProtection;

function BeltOfGiantStrength(bonus) {
	ECItem.call(this, bonus);
	
	this.str = 4;
}
BeltOfGiantStrength.prototype = new ECItem();
BeltOfGiantStrength.prototype.constructor = BeltOfGiantStrength;

function AmuletOfTheHeavens(bonus) {
	ECItem.call(this, bonus);
	
};
AmuletOfTheHeavens.prototype = new ECItem();
AmuletOfTheHeavens.prototype.constructor = AmuletOfTheHeavens;
AmuletOfTheHeavens.prototype.totalAttack = function (wearer, opponent) {
	var atkBonus = 0;
	if (opponent != null) {
		if (opponent.alignment == 'Neutral') { atkBonus = 1; }
		else if (opponent.alignment == 'Evil') { atkBonus = 2; }
	}
	if (wearer != null && wearer.className == 'Paladin') { atkBonus *= 2; }
	return atkBonus;
}
