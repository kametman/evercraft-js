function ECWeapon (bonus, critical) {
	this.damage = 0;
	this.bonus = bonus || 0;
	this.critical = critical || 0;
};
ECWeapon.prototype.weaponDamage = function () {
	return this.damage;
}
ECWeapon.prototype.totalAttack = function () {
	return this.bonus;
};
ECWeapon.prototype.totalDamage = function () {
	return this.weaponDamage() + this.bonus;
};
ECWeapon.prototype.criticalMultiplier = function () {
	return this.critical;
};

function Longsword (bonus) {
	ECWeapon.call(this, bonus);
	this.damage = 5;
};
Longsword.prototype = new ECWeapon();
Longsword.prototype.constructor = Longsword;

function Waraxe(bonus) {
	ECWeapon.call(this, bonus, 1);
	this.damage = 6;
}
Waraxe.prototype = new ECWeapon();
Waraxe.prototype.constructor = Waraxe;

function ElvenLongsword(bonus) {
	Longsword.call(this, bonus);
	this.damage = new Longsword().damage;
}
ElvenLongsword.prototype = new Longsword();
ElvenLongsword.prototype.constructor = ElvenLongsword;
ElvenLongsword.prototype.totalAttack = function (wielder, opponent) {
	var weaponBonus = 1;
	if (wielder != null && wielder.race == 'Elf') { weaponBonus = 2; }
	if (opponent != null && opponent.race == 'Orc') { weaponBonus = 2; }
	if (wielder != null && opponent != null && wielder.race == 'Elf' && opponent.race == 'Orc') { weaponBonus = 5; }
	return this.bonus + weaponBonus;
};
ElvenLongsword.prototype.totalDamage = function (wielder, opponent) {
	var dmgBonus = 1;
	if (wielder != null && wielder.race == 'Elf') { dmgBonus  = 2; }
	if (opponent != null && opponent.race == 'Orc') { dmgBonus = 2; }
	if (wielder != null && opponent != null && wielder.race == 'Elf' && opponent.race == 'Orc') { dmgBonus = 5; }
	return this.damage + this.bonus + dmgBonus;	
};


function Nunchucks(bonus) {
	ECWeapon.call(this, bonus, 0);
	this.damage = 6;
};
Nunchucks.prototype = new ECWeapon();
Nunchucks.prototype.constructor = Nunchucks;
Nunchucks.prototype.totalAttack = function (wielder) {
	var weaponBonus = 0;
	if (wielder.className != 'Monk') { weaponBonus = -4; }
	return this.bonus + weaponBonus;
};

