function ECCharacter () {
	this.str = 10;
	this.dex = 10;
	this.con = 10;
	this.int = 10;
	this.wis = 10;
	this.cha = 10;
	
	this.race = 'Human';
	this.className = 'None';
	this.alignment = 'Neutral';

	this.baseHitPoints = 5;
	this.hitPoints = 5;
	
	this.baseDamage = 1;
	this.critical = 2;
	this.criticalRange = 20;
	this.ignoreDex = false;
	
	this.experience = 0;
	this.level = 1;
	
	this.items = [];
};

ECCharacter.prototype.setName = function (name) {
	this.name = name;
};
ECCharacter.prototype.setAlignment = function (alignment) {
	if (alignment == 'Good' || alignment == 'Neutral' || alignment == 'Evil') {
		this.alignment = alignment;
	}
	else { throw new Error('Invalid alignment'); }
};

Object.defineProperty(ECCharacter.prototype, 'str', {
	get: function () {
		var retVal = this._str;
		for (i = 0; i < this.items.length; i++) { retVal += this.items[i].totalStr(); }
		return retVal; 
	},
	set: function (newValue) {
		this._str = newValue;
	}
});
Object.defineProperty(ECCharacter.prototype, 'con', {
	get: function () { return this._con; },
	set: function (newValue) {
		this._con = newValue;
		this.levelUp();
	}
});
Object.defineProperty(ECCharacter.prototype, 'race', {
	get: function () { return this._race; },
	set: function (newValue) {
		this._race = newValue;
		this.levelUp();
	}
});
Object.defineProperty(ECCharacter.prototype, 'armor', {
	get: function () { return this._armor; },
	set: function (newValue) {
		if (newValue.canEquip(this)) { this._armor = newValue; }
		else { throw new Error('Cannot equip armor.'); }
	}
});

ECCharacter.prototype.strMod = function () {
	var racialBonus = 0;
	if (this.race == 'Orc') { racialBonus = 2;}
	else if (this.race == 'Halfling') { racialBonus = -1; }
	return Math.floor((this.str - 10) / 2) + racialBonus;	
};
ECCharacter.prototype.dexMod = function () {
	var racialBonus = 0;
	if (this.race == 'Elf') { racialBonus = 1; }
	else if (this.race == 'Halfling') { racialBonus = 1; }
	return Math.floor((this.dex - 10) / 2) + racialBonus;	
};
ECCharacter.prototype.conMod = function () {
	var racialBonus = 0;
	if (this.race == 'Dwarf') { racialBonus = 1; }
	else if (this.race == 'Elf') { racialBonus = -1; }
	return Math.floor((this.con - 10) / 2) + racialBonus;	
};
ECCharacter.prototype.intMod = function () {
	var racialBonus = 0;
	if (this.race == 'Orc') { racialBonus = -1;}
	
	return Math.floor((this.int - 10) / 2) + racialBonus;	
};
ECCharacter.prototype.wisMod = function () {
	var racialBonus = 0;
	if (this.race == 'Orc') { racialBonus = -1;}
	
	return Math.floor((this.wis - 10) / 2) + racialBonus;	
};
ECCharacter.prototype.chaMod = function () {
	var racialBonus = 0;
	if (this.race == 'Orc') { racialBonus = -1;}
	else if (this.race == 'Dwarf') { racialBonus = -1; }
	
	return Math.floor((this.cha - 10) / 2) + racialBonus;	
};

ECCharacter.prototype.racialAttackBonus = function (opponent) {
	var racialBonus = 0;
	if (this.race == 'Dwarf' && opponent != null && opponent.race == 'Orc') { racialBonus = 2; }
	return racialBonus;
};
ECCharacter.prototype.equipmentBonus = function (opponent) {
	var bonus = this.weapon ? this.weapon.totalAttack(this, opponent) : 0;
	bonus += this.armor ? this.armor.totalAttack(this) : 0;
	for (i = 0; i < this.items.length; i++) { bonus = this.items[i].totalAttack(this, opponent); }
	return bonus;	
}
ECCharacter.prototype.attackBonus = function (opponent) {
	return parseInt(this.level / 2) + this.strMod();
};

ECCharacter.prototype.baseArmorClass = function (opponent) {
	var baseAC = 10;
	if (this.race == 'Orc') { baseAC += 2; }
	else if (this.race == 'Elf' && opponent != null && opponent.race == 'Orc') { baseAC += 2; }
	else if (this.race == 'Halfling' && opponent != null && opponent.race != 'Halfling') { baseAC += 2; }
	return baseAC;
};
ECCharacter.prototype.armorClass = function (ignoreDex, opponent) {
	var armor = this.armor ? this.armor.totalArmor(this) : 0;
	for (i = 0; i < this.items.length; i++ ) { armor += this.items[i].totalArmor(); }
	if (ignoreDex && this.dexMod() > 0) { return this.baseArmorClass() + armor; }
	else { return this.baseArmorClass(opponent) + this.dexMod() + armor; }
};

ECCharacter.prototype.racialDamageBonus = function (opponent) {
	var racialBonus = 0;
	if (this.race == 'Dwarf' && opponent != null && opponent.race == 'Orc') { racialBonus = 2; }
	return racialBonus;
}
ECCharacter.prototype.weaponDamage = function (wielder, opponent) {
	if (this.weapon != null) { return this.weapon.totalDamage(wielder, opponent); }
	else { return this.baseDamage; }
}
ECCharacter.prototype.damageDealt = function (opponent) {
	return this.weaponDamage(this, opponent) + this.strMod();
};
ECCharacter.prototype.criticalMultiplier = function () {
	return this.critical;
}

ECCharacter.prototype.attack = function (opponent, roll) {
	if (roll < 1 || roll > 20) { throw new Error('Attack roll must be between 1 and 20'); }
	
	var rollWithMods = roll + this.attackBonus(opponent) + this.equipmentBonus(opponent) + this.racialAttackBonus(opponent);
	var success = rollWithMods > opponent.armorClass(this.ignoreDex, this) || roll == 20;
	var damage = this.damageDealt(opponent) + this.racialDamageBonus(opponent);
	if (damage < 1) { damage = 1; }
	var critRange = this.criticalRange;
	if (this.race == 'Elf') { critRange -= 1; }
	var critMultiplier = this.criticalMultiplier(opponent);
	if (this.weapon != null) { critMultiplier += this.weapon.criticalMultiplier(); }
	if (success && roll >= critRange) { opponent.takeDamage(damage * critMultiplier); }
	else if (success) { 
		opponent.takeDamage(damage); 
		this.experience += 10;
	}
	return success;
};
ECCharacter.prototype.takeDamage = function(damage) {
	this.hitPoints -= damage;
}

ECCharacter.prototype.gainsXP = function (xp) {
	this.experience += xp;
	var newLevel = 1 + (this.experience / 1000);
	if (this.level != newLevel) {
		this.level = newLevel;
		this.levelUp(); 
	}
}
ECCharacter.prototype.levelUp = function () {
	this.hitPoints = this.baseHitPoints + this.conMod();
	if (this.race == 'Dwarf' && this.conMod() > 0) { this.hitPoints += this.conMod(); }
	if (this.hitPoints < 1) { this.hitPoints = 1; }
	this.hitPoints *= this.level;
}





