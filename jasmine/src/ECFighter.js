function ECFighter () {
	ECCharacter.call();
	
	this.className = 'Fighter';
	
	this.baseHitPoints = 10;
	this.hitPoints = 10;
};

ECFighter.prototype = new ECCharacter();
ECFighter.prototype.constructor = ECFighter;

ECFighter.prototype.attackBonus = function (opponent) {
	return this.level + this.strMod() + this.racialAttackBonus(opponent);
}


