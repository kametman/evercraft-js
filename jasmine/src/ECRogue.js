function ECRogue () {
	ECCharacter.call();
	
	this.className = 'Rogue';
	
	this.critical = 3;
	this.ignoreDex = true;
};

ECRogue.prototype = new ECCharacter();
ECRogue.prototype.constructor = ECRogue;

ECRogue.prototype.setAlignment = function (alignment) {
	if (alignment == 'Neutral' || alignment == 'Evil') {
		this.alignment = alignment;
	}
	else { throw new Error('Invalid alignment'); }
};

ECRogue.prototype.attackBonus = function (opponent) {
	return parseInt(this.level / 2) + this.dexMod() + this.racialAttackBonus(opponent);
}

